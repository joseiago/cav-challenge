# API

- `GET /cav`
  - Return the list of available CAVs.
  - Ex: ```[
    {
        "id": 1,
        "name": "Botafogo"
    },
    {
        "id": 2,
        "name": "Barra da Tijuca"
    },
    {
        "id": 3,
        "name": "Norte Shopping"
    }]```
- `GET /cav/{cavId}`
  - Return the list of available times of a CAVs separated by inspections and visits.
  - Ex: ```{
    "inspections": {
        "2019-07-17": [
            "15",
            "16",
            "17",
            "10",
            "12",
            "14"
        ],
        "2019-07-18": [
            "12",
            "13",
            "14",
            "15",
            "16",
            "17",
            "10",
            "11"
        ]
    },
    "visits": {
        "2019-07-17": [
            "12",
            "15",
            "16",
            "17",
            "10"
        ],
        "2019-07-18": [
            "16",
            "17",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15"
        ]
    }
}```
- `POST /cav/{cavId}/inspection`
  - Allows scheduling an inspection 
  - Ex of content: ```{
"carId": 2,
"date": "2019-07-17",
"hour": "13" 
}```
- `POST /cav/{cavId}/visit`
  - Allows scheduling a visit 
  - Ex of content: ```{
"carId": 2,
"date": "2019-07-17",
"hour": "13" 
}```

# Run

You can run the project with the available Dockerfile:
```bash
docker build -t cav-challenge-docker .
docker run -d -p 5000:5000 cav-challenge-docker 
```
Also you can install Go on your machine and run the application locally with:
```bash
go build -o main .
./main
```
# Test
There are some tests available in the project which were created using the native lib of Go. You can run them with the follow command:
```
go test ./...
``` 
