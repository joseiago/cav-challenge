FROM golang:1.11

LABEL maintainer="Iago Araujo <joseiago.p.a@gmail.com>"

WORKDIR /usr/local/go/src/bitbucket.org/joseiago/cav-challenge

COPY . .

RUN go build -o main .

EXPOSE 5000

ENTRYPOINT [ "./main" ]
