package controllers

import (
	"encoding/json"
	"strconv"

	"bitbucket.org/joseiago/cav-challenge/models"
	"bitbucket.org/joseiago/cav-challenge/service"
	"github.com/gin-gonic/gin"
	"github.com/pkg/errors"
)

func SetupCav(r gin.IRouter) {
	c := &cavController{}

	cavs := r.Group("/cav")
	{
		cavs.GET("/", c.GetAvailableCavs)
		cav := cavs.Group("/:cavId")
		{
			cav.GET("/", c.GetAvailableTimes)
			cav.POST("/inspection", c.InspectCar)
			cav.POST("/visit", c.VisitCar)
		}
	}
}

type cavController struct {
}

func (c *cavController) GetAvailableCavs(g *gin.Context) {
	cavManager := service.NewCavManager()

	g.JSON(200, cavManager.ListCavs())
}

func (c *cavController) GetAvailableTimes(g *gin.Context) {
	cavIdStr := g.Param("cavId")
	cavId, err := strconv.Atoi(cavIdStr)
	if err != nil {
		g.JSON(500, gin.H{"error": err.Error()})
		return
	}

	cavManager := service.NewCavManager()

	availableTimes, err := cavManager.GetAvailableTime(cavId)
	if err != nil {
		g.JSON(404, gin.H{"error": err.Error()})
		return
	}

	g.JSON(200, availableTimes)
}

func (c *cavController) InspectCar(g *gin.Context) {
	cavIdStr := g.Param("cavId")
	cavId, err := strconv.Atoi(cavIdStr)
	if err != nil {
		g.JSON(500, gin.H{"error": err.Error()})
		return
	}

	payload, err := c.schedulingDecode(g)
	if err != nil {
		c.errorResponse(g, err)
		return
	}

	cavManager := service.NewCavManager()
	err = cavManager.ScheduleInspection(cavId, payload)
	if err != nil {
		g.JSON(404, gin.H{"error": err.Error()})
		return
	}

	g.Status(201)
}

func (c *cavController) VisitCar(g *gin.Context) {
	cavIdStr := g.Param("cavId")
	cavId, err := strconv.Atoi(cavIdStr)
	if err != nil {
		g.JSON(500, gin.H{"error": err.Error()})
		return
	}

	payload, err := c.schedulingDecode(g)
	if err != nil {
		c.errorResponse(g, err)
		return
	}

	cavManager := service.NewCavManager()
	err = cavManager.ScheduleVisit(cavId, payload)
	if err != nil {
		g.JSON(404, gin.H{"error": err.Error()})
		return
	}

	g.Status(201)
}

func (c *cavController) errorResponse(g *gin.Context, err error) {
	switch errors.Cause(err).(type) {
	case models.EmptyFieldError:
		g.JSON(400, gin.H{"error": err.Error()})
	case models.SchedulingError:
		g.JSON(400, gin.H{"error": err.Error()})
	case models.NotFoundError:
		g.JSON(404, gin.H{"error": err.Error()})
	default:
		g.JSON(500, gin.H{"error": err.Error()})
	}
}

func (c *cavController) schedulingDecode(g *gin.Context) (*models.Scheduling, error) {
	decoder := json.NewDecoder(g.Request.Body)
	var payload *models.Scheduling
	if err := decoder.Decode(&payload); err != nil {
		return nil, err
	}
	return payload, nil
}
