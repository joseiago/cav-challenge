package main

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"os"

	"bitbucket.org/joseiago/cav-challenge/clients"
	"bitbucket.org/joseiago/cav-challenge/controllers"
	"bitbucket.org/joseiago/cav-challenge/models"
	"github.com/Sirupsen/logrus"
	"github.com/gin-gonic/gin"
)

func main() {
	r := gin.New()
	r.Use(gin.Logger())
	initStorage()
	setup(r)

	var handler http.Handler = r
	if err := http.ListenAndServe(":5000", handler); err != nil {
		logrus.Fatal("Cannot Listen -> ", err.Error())
	}
}

func initStorage() {
	cavStorage := clients.NewCavStorage()
	carStorage := clients.NewCarStorage()

	jsonFile, _ := os.Open("./data/cav.json")
	byteValue, _ := ioutil.ReadAll(jsonFile)

	var cavs []*models.Cav
	json.Unmarshal(byteValue, &cavs)
	cavStorage.SaveCavs(cavs)

	jsonFile, _ = os.Open("./data/cars.json")
	byteValue, _ = ioutil.ReadAll(jsonFile)

	var cars []*models.Car
	json.Unmarshal(byteValue, &cars)
	carStorage.SaveCars(cars)

	jsonFile, _ = os.Open("./data/calendar.json")
	byteValue, _ = ioutil.ReadAll(jsonFile)

	var calendar []*models.Calendar
	json.Unmarshal(byteValue, &calendar)
	for _, c := range calendar {
		cav, _ := cavStorage.GetCav(c.CavId)
		cav.CavTimes = c.CavTimes
	}
}

func setup(r gin.IRouter) {
	controllers.SetupCav(r)
}
