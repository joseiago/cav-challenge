package service

import (
	"testing"

	"bitbucket.org/joseiago/cav-challenge/clients"
	"bitbucket.org/joseiago/cav-challenge/models"
)

func TestScheduleInspect(T *testing.T) {
	cav := &models.Cav{
		ID:   1,
		Name: "Botafogo",
	}
	saveCav(cav)

	m := NewCavManager()
	scheduling := &models.Scheduling{
		CarId: 1,
	}
	err := m.ScheduleInspection(1, scheduling)
	if err.Error() != "Cannot find car with id: 1" {
		T.Error("Error message is wrong")
	}

	car := &models.Car{
		ID: 1,
	}
	saveCar(car)
	err = m.ScheduleInspection(1, scheduling)
	if err.Error() != "Date can not be empty" {
		T.Error("Error message is wrong")
	}

	scheduling.Date = "2"
	err = m.ScheduleInspection(1, scheduling)
	if err.Error() != "Hour can not be empty" {
		T.Error("Error message is wrong")
	}

	scheduling.Hour = "2"
	err = m.ScheduleInspection(1, scheduling)
	if err.Error() != "Error scheduling a time: date isn't available" {
		T.Error("Error message is wrong")
	}

	cav.Inspections = make(map[string]models.CarsHour)
	carsHour := make(models.CarsHour)
	carsHour["2"] = 0
	carsHour["3"] = 2
	cav.Inspections["2"] = carsHour
	updateCav(cav)

	err = m.ScheduleInspection(1, scheduling)
	if err != nil {
		T.Error("Should not return an error")
	}

	scheduling.Hour = "3"
	err = m.ScheduleInspection(1, scheduling)
	if err.Error() != "Error scheduling a time: hour isn't available" {
		T.Error("Error message is wrong")
	}
}

func TestScheduleVisit(T *testing.T) {
	cav := &models.Cav{
		ID:   2,
		Name: "Flamengo",
	}
	saveCav(cav)

	m := NewCavManager()
	scheduling := &models.Scheduling{
		CarId: 2,
	}
	err := m.ScheduleVisit(2, scheduling)
	if err.Error() != "Cannot find car with id: 2" {
		T.Error("Error message is wrong")
	}

	car := &models.Car{
		ID: 2,
	}
	saveCar(car)
	err = m.ScheduleVisit(2, scheduling)
	if err.Error() != "Date can not be empty" {
		T.Error("Error message is wrong")
	}

	scheduling.Date = "2"
	err = m.ScheduleVisit(2, scheduling)
	if err.Error() != "Hour can not be empty" {
		T.Error("Error message is wrong")
	}

	scheduling.Hour = "2"
	err = m.ScheduleVisit(2, scheduling)
	if err.Error() != "Error scheduling a time: date isn't available" {
		T.Error("Error message is wrong")
	}

	cav.Visits = make(map[string]models.CarsHour)
	carsHour := make(models.CarsHour)
	carsHour["2"] = 0
	carsHour["3"] = 2
	cav.Visits["2"] = carsHour
	updateCav(cav)

	err = m.ScheduleVisit(2, scheduling)
	if err != nil {
		T.Error("Should not return an error")
	}

	scheduling.Hour = "3"
	err = m.ScheduleVisit(2, scheduling)
	if err.Error() != "Error scheduling a time: hour isn't available" {
		T.Error("Error message is wrong")
	}
}

func saveCav(cav *models.Cav) {
	storage := clients.NewCavStorage()
	cavs := make([]*models.Cav, 0, 1)

	cavs = append(cavs, cav)
	storage.SaveCavs(cavs)
}

func saveCar(car *models.Car) {
	storage := clients.NewCarStorage()
	cars := make([]*models.Car, 0, 1)

	cars = append(cars, car)
	storage.SaveCars(cars)
}

func updateCav(cav *models.Cav) {
	storage := clients.NewCavStorage()
	storage.UpdateCav(cav)
}
