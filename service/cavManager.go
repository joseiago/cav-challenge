package service

import (
	"bitbucket.org/joseiago/cav-challenge/clients"
	"bitbucket.org/joseiago/cav-challenge/models"
)

type CavManager interface {
	ListCavs() []*models.Cav
	GetAvailableTime(cavId int) (*models.AvailableTime, error)
	ScheduleVisit(cavId int, scheduling *models.Scheduling) error
	ScheduleInspection(cavId int, scheduling *models.Scheduling) error
}

type cavManager struct {
	cavStorage clients.CavStorage
	carStorage clients.CarStorage
}

func NewCavManager() CavManager {
	return &cavManager{
		cavStorage: clients.NewCavStorage(),
		carStorage: clients.NewCarStorage(),
	}
}

func (m *cavManager) ListCavs() []*models.Cav {
	return m.cavStorage.GetAll()
}

func (m *cavManager) GetAvailableTime(cavId int) (*models.AvailableTime, error) {
	cav, err := m.cavStorage.GetCav(cavId)
	if err != nil {
		return nil, err
	}

	availableTime := &models.AvailableTime{
		Inspections: m.availableFromMap(cav.Inspections),
		Visits:      m.availableFromMap(cav.Visits),
	}

	return availableTime, nil
}

func (m *cavManager) availableFromMap(times map[string]models.CarsHour) map[string][]string {
	availableTime := make(map[string][]string)
	for date, carHour := range times {
		availableHours := make([]string, 0, len(carHour))
		for hour, car := range carHour {
			if car < 1 {
				availableHours = append(availableHours, hour)
			}
		}
		availableTime[date] = availableHours
	}
	return availableTime
}

func (m *cavManager) ScheduleVisit(cavId int, scheduling *models.Scheduling) error {
	cav, err := m.cavStorage.GetCav(cavId)
	if err != nil {
		return err
	}

	err = m.validateScheduling(scheduling)
	if err != nil {
		return err
	}

	err = m.scheduleTime(scheduling, cav.Visits)
	if err != nil {
		return err
	}

	return nil
}

func (m *cavManager) ScheduleInspection(cavId int, scheduling *models.Scheduling) error {
	cav, err := m.cavStorage.GetCav(cavId)
	if err != nil {
		return err
	}

	err = m.validateScheduling(scheduling)
	if err != nil {
		return err
	}

	err = m.scheduleTime(scheduling, cav.Inspections)
	if err != nil {
		return err
	}

	return nil
}

func (m *cavManager) scheduleTime(scheduling *models.Scheduling, cavData map[string]models.CarsHour) error {
	date, ok := cavData[scheduling.Date]
	if !ok {
		return models.NewSchedulingError("date isn't available")
	}

	hour, ok := date[scheduling.Hour]
	if !ok || hour > 0 {
		return models.NewSchedulingError("hour isn't available")
	}

	date[scheduling.Hour] = scheduling.CarId
	return nil
}

func (m *cavManager) validateScheduling(scheduling *models.Scheduling) error {
	_, err := m.carStorage.GetCar(scheduling.CarId)
	if err != nil {
		return err
	}

	if scheduling.Date == "" {
		return models.NewEmptyFieldErrorf("%v can not be empty", "Date")
	}

	if scheduling.Hour == "" {
		return models.NewEmptyFieldErrorf("%v can not be empty", "Hour")
	}

	return nil
}
