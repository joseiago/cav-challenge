package clients

import (
	"testing"

	"bitbucket.org/joseiago/cav-challenge/models"
)

func TestSaveCars(T *testing.T) {
	storage := NewCarStorage()
	cars := make([]*models.Car, 0, 1)

	storage.SaveCars(cars)
	car := &models.Car{
		ID: 1,
	}
	cars = append(cars, car)
	storage.SaveCars(cars)

	carSaved, err := storage.GetCar(1)
	if err != nil {
		T.Error("Get car should return the saved car")
	}

	if car != carSaved {
		T.Error("Error getting the saved car")
	}
}

func TestNotFoundCar(T *testing.T) {
	storage := NewCarStorage()

	_, err := storage.GetCar(2)
	if err == nil {
		T.Error("Should return not found error")
	}

	if err.Error() != "Cannot find car with id: 2" {
		T.Error("Error message is wrong")
	}
}
