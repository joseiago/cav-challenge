package clients

import "bitbucket.org/joseiago/cav-challenge/models"

var carDb CarStorage

type CarStorage interface {
	GetCar(id int) (*models.Car, error)
	SaveCars(cars []*models.Car)
}

type carStorage struct {
	carsById map[int]*models.Car
}

func NewCarStorage() CarStorage {
	if carDb == nil {
		carDb = &carStorage{
			carsById: make(map[int]*models.Car),
		}
	}
	return carDb
}

func (s *carStorage) SaveCars(cars []*models.Car) {
	for _, car := range cars {
		s.carsById[car.ID] = car
	}
}

func (s *carStorage) GetCar(id int) (*models.Car, error) {
	if value, ok := s.carsById[id]; ok {
		return value, nil
	}
	return nil, models.NewNotFoundErrorf("Cannot find car with id: %v", id)
}
