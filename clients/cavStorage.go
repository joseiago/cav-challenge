package clients

import "bitbucket.org/joseiago/cav-challenge/models"

var cavDb CavStorage

type CavStorage interface {
	GetAll() []*models.Cav
	GetCav(id int) (*models.Cav, error)
	UpdateCav(newCav *models.Cav) error
	SaveCavs(cavs []*models.Cav)
}

type cavStorage struct {
	cavsById map[int]*models.Cav
}

func NewCavStorage() CavStorage {
	if cavDb == nil {
		cavDb = &cavStorage{
			cavsById: make(map[int]*models.Cav),
		}
	}
	return cavDb
}

func (s *cavStorage) GetAll() []*models.Cav {
	cavs := make([]*models.Cav, 0, len(s.cavsById))
	for _, value := range s.cavsById {
		cavs = append(cavs, value)
	}

	return cavs
}

func (s *cavStorage) SaveCavs(cavs []*models.Cav) {
	for _, cav := range cavs {
		s.cavsById[cav.ID] = cav
	}
}

func (s *cavStorage) GetCav(id int) (*models.Cav, error) {
	if value, ok := s.cavsById[id]; ok {
		return value, nil
	}
	return nil, models.NewNotFoundErrorf("Cannot find cav with id: %v", id)
}

func (s *cavStorage) UpdateCav(newCav *models.Cav) error {
	_, err := s.GetCav(newCav.ID)
	if err != nil {
		return err
	}

	s.cavsById[newCav.ID] = newCav
	return nil
}
