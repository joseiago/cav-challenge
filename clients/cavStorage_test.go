package clients

import (
	"testing"

	"bitbucket.org/joseiago/cav-challenge/models"
)

func TestSaveCavs(T *testing.T) {
	storage := NewCavStorage()
	cavs := make([]*models.Cav, 0, 1)

	cav := &models.Cav{
		ID:   1,
		Name: "Botafogo",
	}
	cavs = append(cavs, cav)
	storage.SaveCavs(cavs)

	cavSaved, err := storage.GetCav(1)
	if err != nil {
		T.Error("Get cav should return the saved cav")
	}

	if cav != cavSaved {
		T.Error("Error getting the saved cav")
	}
}

func TestCavNotFound(T *testing.T) {
	storage := NewCavStorage()

	_, err := storage.GetCav(2)
	if err == nil {
		T.Error("Should return not found error")
	}

	if err.Error() != "Cannot find cav with id: 2" {
		T.Error("Error message is wrong")
	}
}

func TestGetAllCavs(T *testing.T) {
	storage := NewCavStorage()
	beforeSavedCavs := storage.GetAll()
	cavs := make([]*models.Cav, 0, 1)

	cav := &models.Cav{
		ID:   2,
		Name: "Barra",
	}
	cavs = append(cavs, cav)
	storage.SaveCavs(cavs)

	currentSavedCavs := storage.GetAll()
	if len(currentSavedCavs) != len(beforeSavedCavs)+1 {
		T.Error("Number of saved cavs is wrong")
	}
}

func TestUpdateCav(T *testing.T) {
	storage := NewCavStorage()
	cavs := make([]*models.Cav, 0, 1)

	cav := &models.Cav{
		ID:   3,
		Name: "Flamengo",
	}
	cavs = append(cavs, cav)
	storage.SaveCavs(cavs)

	cav.Name = "Catete"
	err := storage.UpdateCav(cav)
	if err != nil {
		T.Error("Should not return an error")
	}

	cav = &models.Cav{
		ID:   4,
		Name: "Flamengo",
	}
	err = storage.UpdateCav(cav)
	if err == nil {
		T.Error("Should return not found error")
	}
}
