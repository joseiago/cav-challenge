package models

type Scheduling struct {
	CarId int    `json:"carId"`
	Date  string `json:"date"`
	Hour  string `json:"hour"`
}
