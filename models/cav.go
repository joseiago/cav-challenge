package models

type Cav struct {
	ID       int    `json:"id"`
	Name     string `json:"name"`
	CavTimes `json:"-"`
}

type CavTimes struct {
	Inspections map[string]CarsHour
	Visits      map[string]CarsHour
}

type CarsHour map[string]int

type AvailableTime struct {
	Inspections map[string][]string `json:"inspections"`
	Visits      map[string][]string `json:"visits"`
}
