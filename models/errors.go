package models

import "fmt"

type SchedulingError struct {
	msg string
}

func (e SchedulingError) Error() string {
	return e.msg
}

func NewSchedulingError(msg string) error {
	return SchedulingError{fmt.Sprintf("Error scheduling a time: %v", msg)}
}

type EmptyFieldError struct {
	msg string
}

func (e EmptyFieldError) Error() string {
	return e.msg
}

func NewEmptyFieldErrorf(msgFormat string, args ...interface{}) error {
	return EmptyFieldError{fmt.Sprintf(msgFormat, args...)}
}

type NotFoundError struct {
	msg string
}

func (e NotFoundError) Error() string {
	return e.msg
}

func NewNotFoundErrorf(msgFormat string, args ...interface{}) error {
	return NotFoundError{fmt.Sprintf(msgFormat, args...)}
}
